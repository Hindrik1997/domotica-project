using System;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;


namespace Domoticapp_Rescue_Mission
{
    class CommandsFragment : Fragment
    {
        Button bierButton;
        View commandsView;
        TimePicker timePicker;
        CheckBox bierDelayCheckbox;
        Button fanButton;
        Button lampButton;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);

            commandsView = inflater.Inflate(Resource.Layout.commands_layout, container, false);

            bierButton = commandsView.FindViewById<Button>(Resource.Id.bierOrder);
            timePicker = commandsView.FindViewById<TimePicker>(Resource.Id.bierTimePicker);
            bierDelayCheckbox = commandsView.FindViewById<CheckBox>(Resource.Id.bierDelayCheckbox);
            fanButton = commandsView.FindViewById<Button>(Resource.Id.fanButton);
            lampButton = commandsView.FindViewById<Button>(Resource.Id.lampButton);

            fanButton.Text = Resources.GetString(Resource.String.fan) + " " + (GlobalVariables.fanEnabled ? "uit" : "aan");
            lampButton.Text = Resources.GetString(Resource.String.fan) + " " + (GlobalVariables.lampEnabled ? "uit" : "aan");

            bierDelayCheckbox.Click += (o, e) =>
            {
                if (bierDelayCheckbox.Checked) timePicker.Visibility = ViewStates.Visible;
                else timePicker.Visibility = ViewStates.Gone;
            };

            bierButton.Click += (sender, e) =>
            {
                if (!GlobalVariables.enableGroupAssignment)
                {
                    if (bierDelayCheckbox.Checked)
                    {
                        GlobalVariables.communication.SendMessage(new Message(Arduino.Phone, Arduino.Meetstation, ConnectionUtils.CommandSetToByteArray(new char[] { 'b', 't' }, Convert.ToInt16(timePicker.Hour * 60 + timePicker.Minute))));
                    }

                    else {
                        GlobalVariables.communication.SendMessage(new Message(Arduino.Phone, Arduino.Meetstation, new byte[] { (byte)'b', (byte)'o' }));
                        Console.WriteLine("Ordering beer.");
                    }
                }
                else
                {
                    AlertDialog dialog = new AlertDialog.Builder(Activity).Create();
                    dialog.SetMessage(Resources.GetString(Resource.String.groupAssignmentActive));
                    dialog.Show();
                }
            };

            fanButton.Click += (o, e) =>
            {
                GlobalVariables.fanEnabled = !GlobalVariables.fanEnabled;
                GlobalVariables.communication.SendMessage(new Message(Arduino.Phone, Arduino.Server, ConnectionUtils.CommandSetToByteArray(new char[] { 'm', 'f' }, Convert.ToInt16(GlobalVariables.fanEnabled ? 1 : 0))));
                fanButton.Text = Resources.GetString(Resource.String.fan) + " " + (GlobalVariables.fanEnabled ? "uit" : "aan");
            };

            lampButton.Click += (o, e) =>
            {
                GlobalVariables.lampEnabled = !GlobalVariables.lampEnabled;
                GlobalVariables.communication.SendMessage(new Message(Arduino.Phone, Arduino.Server, ConnectionUtils.CommandSetToByteArray(new char[] { 'm', 'l' }, Convert.ToInt16(GlobalVariables.lampEnabled ? 1 : 0))));
                lampButton.Text = Resources.GetString(Resource.String.fan) + " " + (GlobalVariables.lampEnabled ? "uit" : "aan");
            };

            return commandsView;
        }
    }
}