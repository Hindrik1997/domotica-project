/*
 Name:		MessageSystem.cs
 Created:	1/18/2016 11:07:08 AM
 Author:	Hindrik
*/

using System;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Collections.Generic;

namespace Domoticapp_Rescue_Mission
{
    public static class ConnecionData
    {
        public static int Port = 240;
    }

    

    /// <summary>
    /// Representeert een client connectie
    /// </summary>
    public class ClientConnection
    {
        public bool shouldRun = true;
        public bool shouldReconnect = false;
        public LoginActivity loginActivity;
        public MainActivity mainActivity;
        private Socket socket;
        private IPAddress ip;
        private Queue<Message> OutgoingMessageQueue = new Queue<Message>();
        private Queue<Message> IncomingMessageQueue = new Queue<Message>();
        protected Thread commThread;
        public Arduino CurrentClient;

        public ClientConnection(IPAddress IP, Arduino ThisClient)
        {
            CurrentClient = ThisClient;
            ip = IP;
            Console.WriteLine("Starting ReceiveSend.");
            Console.WriteLine("Socket connected.");
            commThread = new Thread(BackgroundCommunication);
            commThread.Start();
        }

        public void Kill()
        {
            if (socket != null)
            {
                lock (socket)
                {
                    if (socket.Connected)
                    {
                        socket.Shutdown(SocketShutdown.Both);
                        socket.Close();
                    }
                    socket = null;
                    commThread.Abort();
                }
            }
        }

        private void BackgroundCommunication()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Console.WriteLine("Connecting...");
            try
            {
                socket.Connect(ip, 240);
                Console.WriteLine("Sock connected");
            }
            catch (Exception e)
            {
                Console.WriteLine("Socket Execption: " + e.Message);
                Kill();
            };
            socket.NoDelay = true;

            Console.WriteLine("Communication: Online.");
            while (shouldRun)
            {
                if (socket.Connected)
                {
                    if (shouldReconnect)
                    {
                        lock (socket)
                        {
                            socket.Shutdown(SocketShutdown.Both);
                            socket.Close();
                            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                            socket.Connect(ip, 240);
                            shouldReconnect = false;
                        }
                    }

                    if (socket.Available > 0)
                    {
                        byte[] CheckMsg = { 0 };

                        while (CheckMsg[0] != 254)
                            socket.Receive(CheckMsg,1,0);


                        //Check voor inkomende bytes
                        byte[] Header = new byte[3];

                        for (int i = 0; i < 3; ++i)
                        {
                            socket.Receive(Header, i, 1, 0);
                        }
                        byte[] msgData = new byte[Header[2] + 3];

                        msgData[0] = Header[0];
                        msgData[1] = Header[1];
                        msgData[2] = Header[1];

                        for (int i = 0; i < Header[2]; ++i)
                        {
                            socket.Receive(msgData, i + 3, 1, 0);
                        }

                        Message msgrec = new Message(msgData);
                        if (msgrec.receiver == CurrentClient)
                        {
                            IncomingMessageQueue.Enqueue(msgrec);
                            GlobalVariables.fireNewMessage(this);
                            Console.WriteLine("Firing Event");
                        }

                        Console.WriteLine("Data received.");
                    }

                    //En verzend al de messages
                    while (OutgoingMessageQueue != null && OutgoingMessageQueue.Count > 0)
                    {
                        Console.WriteLine("Sending Messages");
                        socket.Send(OutgoingMessageQueue.Dequeue().MsgToSend);
                    }
                }
                else shouldReconnect = true;
            }
        }



        /// <summary>
        /// Hiermee verzend je een message
        /// </summary>
        /// <param name="msg">Message om te verzenden</param>
        public void SendMessage(Message msg)
        {
            OutgoingMessageQueue.Enqueue(msg);
        }


        /// <summary>
        /// Haalt een evt. beschikbare message op. Check dit voor een null ref!
        /// </summary>
        /// <returns>Een message indien er een message is, of null indien er geen is</returns>
        public Message GetMessage()
        {
            if (IncomingMessageQueue != null && IncomingMessageQueue.Count > 0) return IncomingMessageQueue.Dequeue();
            else return null;
        }

    }


    public enum Arduino : byte
    {
        Null,
        Server,
        NXTBuddy,
        Laadstation,
        Meetstation,
        Phone
    }

    public class Message
    {
        public byte[] message;
        public byte[] MsgToSend;
        public Arduino receiver;
        public Arduino sender;
        public byte length;

        /// <summary>
        /// Je maakt een message door eerst de sender in te passen, daarna de receiver en dan de byte[]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="receiver"></param>
        /// <param name="message"></param>
        public Message(Arduino sender, Arduino receiver, byte[] message)
        {
            this.message = message;
            this.receiver = receiver;
            this.sender = sender;
            length = (byte)message.Length;
            Serialize();
        }

        /// <summary>
        /// Voor het decoden van byte arrays naar messages
        /// </summary>
        /// <param name="rawData"></param>
        public Message(byte[]rawData)
        {
            MsgToSend = rawData;
            Deserialize();
        }

        private void Serialize()
        {
            MsgToSend = new byte[length + 4];
            MsgToSend[0] = 254;
            MsgToSend[1] = (byte)sender;
            MsgToSend[2] = (byte)receiver;
            MsgToSend[3] = length;
            Array.Copy(message,0,MsgToSend,4,length);
        }
        private void Deserialize()
        {
            message = new byte[MsgToSend.Length - 3];
            sender = (Arduino)MsgToSend[0];
            receiver = (Arduino)MsgToSend[1];
            length = (byte)(MsgToSend.Length - 3);
            for (int i = 0; i < length; ++i)
            {
                message[i] = MsgToSend[i+3];
            }
        }
    }

    public static class ConnectionUtils
    {
        public static byte[] CommandSetToByteArray(char[] chars, short input)
        {
            List<Byte> Bytes = new List<byte>();

            for (int i = 0; i < chars.Length; ++i)
            {
                Bytes.Add((byte)(chars[i]));
            }
            byte[] intBytes = BitConverter.GetBytes(input);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(intBytes);

            for (int i = 0; i < intBytes.Length; ++i)
            {
                Bytes.Add(intBytes[i]);
            }

            return Bytes.ToArray();
        }

        public static short BytesToShort(byte byte1, byte byte2)
        {
            byte[] bytes = new byte[] { byte1, byte2 };
            return BytesToShort(bytes);
        }

        public static short BytesToShort(byte[] bytes)
        {
            if (BitConverter.IsLittleEndian) Array.Reverse(bytes);

            return BitConverter.ToInt16(bytes, 0);
        }
    }
}