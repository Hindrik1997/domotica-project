using System;
using Android.App;
using Android.Runtime;

namespace Domoticapp_Rescue_Mission
{
    [Application]
    public class GlobalVariables : Application
    {
        public static ClientConnection communication;
        public static bool enableTempSensor = true;
        public static bool enableLightSensor = true;
        public static bool enableGroupAssignment = false;
        public static bool fanEnabled = false;
        public static bool lampEnabled = false;
        public static event EventHandler newMessage;
        public static int analogReadInterval = 500;
        public static short lowerLightBound = 300;
        public static short upperTemperatureBound = 10;

        public GlobalVariables(IntPtr handle, JniHandleOwnership ownerShip) : base(handle, ownerShip)
        {

        }

        public static void fireNewMessage(object o)
        {
            if (newMessage != null) newMessage(o, EventArgs.Empty);
        }

        public override void OnCreate()
        {
            base.OnCreate();
        }
    }
}