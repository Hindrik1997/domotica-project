using System;

using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace Domoticapp_Rescue_Mission
{
    class ConfigFragment : Fragment
    {
        Switch lightSwitch;
        Switch temperatureSwitch;
        Switch groupAssignmentSwitch;
        TextView analogIntervalText;
        SeekBar analogIntervalSeekbar;
        TextView lowerLightBoundText;
        SeekBar lowerLightBoundSeekbar;
        TextView upperTemperatureBoundText;
        SeekBar upperTemperatureBoundSeekbar;
        Button updateBoundsButton;


        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);

            View view = inflater.Inflate(Resource.Layout.config_layout, container, false);

            lightSwitch = view.FindViewById<Switch>(Resource.Id.lightSwitch);
            temperatureSwitch = view.FindViewById<Switch>(Resource.Id.temperatureSwitch);
            groupAssignmentSwitch = view.FindViewById<Switch>(Resource.Id.groupAssignmentSwitch);
            analogIntervalText = view.FindViewById<TextView>(Resource.Id.analogIntervalText);
            analogIntervalSeekbar = view.FindViewById<SeekBar>(Resource.Id.analogIntervalSeekbar);
            upperTemperatureBoundText = view.FindViewById<TextView>(Resource.Id.upperTemperatureBoundText);
            upperTemperatureBoundSeekbar = view.FindViewById<SeekBar>(Resource.Id.upperTemperatureBoundSeekbar);
            lowerLightBoundText = view.FindViewById<TextView>(Resource.Id.lowerLightBoundText);
            lowerLightBoundSeekbar = view.FindViewById<SeekBar>(Resource.Id.lowerLightBoundSeekbar);
            updateBoundsButton = view.FindViewById<Button>(Resource.Id.updateBoundsButton);

            analogIntervalText.Text = Resources.GetString(Resource.String.setAnalogInterval) + " " + GlobalVariables.analogReadInterval;
            upperTemperatureBoundText.Text = Resources.GetString(Resource.String.upperTemperatureBound) + " " + GlobalVariables.upperTemperatureBound;
            lowerLightBoundText.Text = Resources.GetString(Resource.String.lowerLightBound) + " " + GlobalVariables.lowerLightBound;

            lightSwitch.Checked = true;
            temperatureSwitch.Checked = true;
            groupAssignmentSwitch.Checked = false;

            lightSwitch.Click += (o, e) =>
            {
                GlobalVariables.enableLightSensor = lightSwitch.Checked;
            };

            temperatureSwitch.Click += (o, e) =>
            {
                GlobalVariables.enableTempSensor = temperatureSwitch.Checked;
            };

            groupAssignmentSwitch.Click += (o, e) =>
            {
                GlobalVariables.enableGroupAssignment = groupAssignmentSwitch.Checked;
                if (groupAssignmentSwitch.Checked) GlobalVariables.communication.SendMessage(new Message(Arduino.Phone, Arduino.Meetstation, ConnectionUtils.CommandSetToByteArray(new char[] { 'g' }, 1)));
                else GlobalVariables.communication.SendMessage(new Message(Arduino.Phone, Arduino.Meetstation, ConnectionUtils.CommandSetToByteArray(new char[] { 'g' }, 0)));
            };

            analogIntervalSeekbar.ProgressChanged += (o, e) =>
            {
                GlobalVariables.analogReadInterval = analogIntervalSeekbar.Progress + 100;
                analogIntervalText.Text = Resources.GetString(Resource.String.setAnalogInterval) + " " + GlobalVariables.analogReadInterval;
            };

            upperTemperatureBoundSeekbar.ProgressChanged += (o, e) =>
            {
                GlobalVariables.upperTemperatureBound = Convert.ToInt16(upperTemperatureBoundSeekbar.Progress - 20);
                upperTemperatureBoundText.Text = Resources.GetString(Resource.String.upperTemperatureBound) + " " + GlobalVariables.upperTemperatureBound;
            };

            lowerLightBoundSeekbar.ProgressChanged += (o, e) =>
            {
                GlobalVariables.lowerLightBound = Convert.ToInt16(lowerLightBoundSeekbar.Progress);
                lowerLightBoundText.Text = Resources.GetString(Resource.String.lowerLightBound) + " " + GlobalVariables.lowerLightBound;
            };

            updateBoundsButton.Click += (o, e) =>
            {
                GlobalVariables.communication.SendMessage(new Message(Arduino.Phone, Arduino.Server, ConnectionUtils.CommandSetToByteArray(new char[] { 's', 't' }, GlobalVariables.upperTemperatureBound)));
                GlobalVariables.communication.SendMessage(new Message(Arduino.Phone, Arduino.Server, ConnectionUtils.CommandSetToByteArray(new char[] { 's', 'l' }, GlobalVariables.lowerLightBound)));
            };



            return view;
        }

        public override void OnResume()
        {
            System.Console.WriteLine("communication: " + GlobalVariables.communication != null);
            base.OnResume();
        }
    }
}