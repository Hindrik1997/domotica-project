using System.Timers;

using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

namespace Domoticapp_Rescue_Mission
{
    class SensorFragment : Fragment
    {
        CardView lightCard;
        public TextView lightText;
        CardView temperatureCard;
        public TextView temperatureText;
        CardView emptyCard;

        Timer analogReadTimer;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            View view = inflater.Inflate(Resource.Layout.sensor_layout, container, false);

            lightCard = view.FindViewById<CardView>(Resource.Id.lightCard);
            lightText = view.FindViewById<TextView>(Resource.Id.lightText);
            temperatureCard = view.FindViewById<CardView>(Resource.Id.temperatureCard);
            temperatureText = view.FindViewById<TextView>(Resource.Id.temperatureText);
            emptyCard = view.FindViewById<CardView>(Resource.Id.emptyCard);

            analogReadTimer = new Timer(GlobalVariables.analogReadInterval);
            analogReadTimer.AutoReset = false;
            analogReadTimer.Start();

            analogReadTimer.Elapsed += (o, e) =>
            {
                GlobalVariables.communication.SendMessage(new Message(Arduino.Phone, Arduino.Server, new byte[] { (byte)'a', (byte)'t' }));
                GlobalVariables.communication.SendMessage(new Message(Arduino.Phone, Arduino.Server, new byte[] { (byte)'a', (byte)'l' }));

                analogReadTimer.Interval = GlobalVariables.analogReadInterval;
                analogReadTimer.Start();
            };

            return view;
        }

        public override void OnPause()
        {
            analogReadTimer.Stop();
            base.OnPause();
        }

        public override void OnResume()
        {
            base.OnResume();

            //Stuur waardes van GlobalVariables.enable... naar sensorduino

            if (GlobalVariables.enableLightSensor) lightCard.Visibility = ViewStates.Visible;
            else lightCard.Visibility = ViewStates.Gone;

            if (GlobalVariables.enableTempSensor) temperatureCard.Visibility = ViewStates.Visible;
            else temperatureCard.Visibility = ViewStates.Gone;

            if (!GlobalVariables.enableLightSensor && !GlobalVariables.enableTempSensor) emptyCard.Visibility = ViewStates.Visible;
            else emptyCard.Visibility = ViewStates.Gone;

            if(analogReadTimer == null)
            {
                analogReadTimer = new Timer(GlobalVariables.analogReadInterval);
            }
            analogReadTimer.Start();


        }
    }
}