using System;
using System.Net;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Graphics;
using RuntimePermissions;

namespace Domoticapp_Rescue_Mission
{
    [Activity(Label = "Domoticapp", MainLauncher = true, Icon = "@drawable/icon")]
    public class LoginActivity : AppCompatActivity
    {
        IPAddress parsedIP;
        EditText serverIPField;
        bool internetPermission = false;
        FloatingActionButton fab;
        Button connectButton;
        Permission permission;
        ProgressDialog progressDialog;
        CoordinatorLayout parentLayout;

        ~LoginActivity()
        {
            GlobalVariables.newMessage -= HandleNewMessage;
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.Login);

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.loginToolbar);
            SetSupportActionBar(toolbar);

            permission = new Permission(this);
            if (permission.Check(Android.Manifest.Permission.Internet, 0)) internetPermission = true;

            serverIPField = FindViewById<EditText>(Resource.Id.serverIP);
            connectButton = FindViewById<Button>(Resource.Id.connectButton);
            fab = FindViewById<FloatingActionButton>(Resource.Id.fab);
            parentLayout = FindViewById<CoordinatorLayout>(Resource.Id.loginRLayout);


            //manage fragments
            if (FindViewById(Resource.Id.fragment_container) != null)
            {
                if (bundle != null)
                {
                    return;
                }
            }

            fab.Click += (sender, e) =>
            {
                StartConnection("192.168.2.40");

            };

            connectButton.Click += (sender, e) =>
            {
                StartConnection(serverIPField.Text);
            };
        }

        protected override void OnResume()
        {
            {
                if (GlobalVariables.communication == null)
                {
                    if (parsedIP == null)
                    {
                        Intent intent = new Intent(this, typeof(LoginActivity));
                        StartActivity(intent);
                        Finish();
                    }

                    GlobalVariables.communication = new ClientConnection(parsedIP, Arduino.Phone);
                }
                base.OnResume();
            }
        }

        void HandleNewMessage(object sender, EventArgs e)
        {
            Console.WriteLine("message Received");
            Message msg = GlobalVariables.communication.GetMessage();
            if (msg != null)
            {
                switch ((char)msg.message[0])
                {
                    case 'w':
                        SuccesfullyConnected();
                        break;
                }
            }
            else Console.WriteLine("Message is null");
        }

        void StartConnection(string ip)
        {
            if (internetPermission)
            {
                //Probeer de string uit het input field te parsen 
                if (IPAddress.TryParse(ip, out parsedIP))
                {
                    //Probeer een verbinding op te zetten met de server
                    GlobalVariables.communication = new ClientConnection(parsedIP, Arduino.Phone);
                    GlobalVariables.communication.loginActivity = this;
                    GlobalVariables.communication.SendMessage(new Message(Arduino.Phone, Arduino.Server, new byte[] { (byte)'i' }));

                    GlobalVariables.newMessage += HandleNewMessage;

                    progressDialog = new ProgressDialog(this);
                    progressDialog.SetMessage(String.Format("Connecting to {0}.", parsedIP));
                    progressDialog.SetCancelable(true);
                    progressDialog.Indeterminate = true;
                    progressDialog.CancelEvent += (o, args) =>
                    {
                        //Kill de thread waarin de verbinding getest wordt, 
                        //voor wanneer Socket.Connect geen verbinding kan krijgen met de server.
                        GlobalVariables.communication.Kill();

                        Snackbar.Make(fab, Resource.String.connectCancel, Snackbar.LengthLong)
                        .SetAction(Resource.String.emptyInput, (view) => { serverIPField.Text = ""; })
                        .Show();
                    };
                    progressDialog.Show();
                }
            }
            else
            {
                permission.Check(Android.Manifest.Permission.Internet, 0);
            }
        }

        void SuccesfullyConnected()
        {
            RunOnUiThread(() => { progressDialog.Hide(); });
            //Als het gelukt is om een verbinding te maken, ga dan door naar de main activity
            Intent MainIntent = new Intent(this, typeof(MainActivity));
            MainIntent.PutExtra("IP", parsedIP.ToString());
            StartActivity(MainIntent);
        }

        public void ConnectFailed()
        {
            Console.WriteLine("No connection.");
            Snackbar.Make(fab, Resource.String.connectFail, Snackbar.LengthLong)
            .SetAction(Resource.String.emptyInput, (view) => { serverIPField.Text = ""; })
            .Show();
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            if (permissions[0] == Android.Manifest.Permission.Internet && grantResults[0] == Android.Content.PM.Permission.Granted)
            {
                internetPermission = true;
            }
        }


    }
}