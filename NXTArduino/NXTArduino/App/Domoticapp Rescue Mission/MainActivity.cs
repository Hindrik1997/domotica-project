﻿using Android.App;
using Android.Content;
using Android.Views;
using Android.OS;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using Android.Transitions;

using System;
using System.Net;

namespace Domoticapp_Rescue_Mission
{
    [Activity(Label = "Domoticapp", MainLauncher = false, Icon = "@drawable/icon")]
    public class MainActivity : AppCompatActivity
    {
        IPAddress parsedIP;
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        CommandsFragment commandsFragment;
        SensorFragment sensorFragment;
        ConfigFragment configFragment;
        Fragments currentFragment;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            if (Intent.HasExtra("IP")) parsedIP = IPAddress.Parse(Intent.GetStringExtra("IP"));

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            GlobalVariables.communication.mainActivity = this;

            //Stel grenswaarden in
            GlobalVariables.communication.SendMessage(new Message(Arduino.Phone, Arduino.Server, ConnectionUtils.CommandSetToByteArray(new char[] { 's', 't' }, GlobalVariables.upperTemperatureBound)));
            GlobalVariables.communication.SendMessage(new Message(Arduino.Phone, Arduino.Server, ConnectionUtils.CommandSetToByteArray(new char[] { 's', 'l' }, GlobalVariables.lowerLightBound)));

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.navigation);

            commandsFragment = new CommandsFragment();
            sensorFragment = new SensorFragment();
            configFragment = new ConfigFragment();

            navigationView.NavigationItemSelected += (sender, e) => {
                e.MenuItem.SetChecked(true);

                switch (e.MenuItem.ItemId)
                {
                    case Resource.Id.commandsMenu:
                        FragmentManager.BeginTransaction()
                        .SetTransition(FragmentTransit.FragmentFade)
                        .Replace(Resource.Id.fragment_container, commandsFragment)
                        .Commit();
                        currentFragment = Fragments.Commands;
                        break;
                    case Resource.Id.sensorMenu:
                        FragmentManager.BeginTransaction()
                        .SetTransition(FragmentTransit.FragmentFade)
                        .Replace(Resource.Id.fragment_container, sensorFragment)
                        .Commit();
                        currentFragment = Fragments.Sensors;
                        break;
                    case Resource.Id.configMenu:
                        FragmentManager.BeginTransaction()
                        .SetTransition(FragmentTransit.FragmentFade)
                        .Replace(Resource.Id.fragment_container, configFragment)
                        .Commit();
                        currentFragment = Fragments.Config;
                        break;
                }
                SupportActionBar.Title = e.MenuItem.TitleFormatted.ToString();
                //react to click here and swap fragments or navigate
                drawerLayout.CloseDrawers();
            };
            FragmentTransaction fTransaction = FragmentManager.BeginTransaction();
            fTransaction.Replace(Resource.Id.fragment_container, commandsFragment).Commit();
            navigationView.Menu.GetItem(0).SetChecked(true);
            SupportActionBar.Title = navigationView.Menu.GetItem(0).TitleFormatted.ToString();
            currentFragment = Fragments.Commands;

            GlobalVariables.newMessage += HandleNewMessage;
        }

        void HandleNewMessage(object sender, EventArgs e)
        {
            Console.WriteLine("message Received");
            Message msg = GlobalVariables.communication.GetMessage();
            short value;
            bool state;
            if (msg != null)
            {
                switch ((char)msg.message[0])
                {
                    case 'v':
                        value = ConnectionUtils.BytesToShort(msg.message[2], msg.message[3]);
                        switch ((char)msg.message[1])
                        {
                            case 't':
                                sensorFragment.temperatureText.Text = value + " °C";
                                break;
                            case 'l':
                                sensorFragment.lightText.Text = value + " lux";
                                break;
                        }
                        break;
                    case 'h':
                        state = ConnectionUtils.BytesToShort(msg.message[2], msg.message[3]) == 1;
                        switch ((char)msg.message[1])
                        {
                            case 'f':
                                GlobalVariables.fanEnabled = state;
                                break;

                            case 'l':
                                GlobalVariables.lampEnabled = state;
                                break;
                        }
                        break;
                }
            }
            else Console.WriteLine("Message is null");
        }

        protected override void OnPause()
        {
            GlobalVariables.newMessage -= HandleNewMessage;
            base.OnPause();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.toolbar_menu, menu);
            return true;
        }

        protected override void OnResume()
        {
            if (GlobalVariables.communication == null)
            {
                if(parsedIP == null)
                {
                    Intent intent = new Intent(this, typeof(LoginActivity));
                    StartActivity(intent);
                    Finish();
                }

                GlobalVariables.newMessage += HandleNewMessage;

                GlobalVariables.communication = new ClientConnection(parsedIP, Arduino.Phone);
            }

            base.OnResume();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            Console.WriteLine("Menu: \t" + item.ItemId);
            Console.WriteLine("Home: \t" + Resource.Id.home);
            Console.WriteLine("Help: \t" + Resource.Id.helpButton);

            switch (item.ItemId)
            {
                case global::Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    break;

                case Resource.Id.helpButton:
                    Android.App.AlertDialog dialog = new Android.App.AlertDialog.Builder(this).Create();
                    switch (currentFragment)
                    {
                        case Fragments.Commands:
                            dialog.SetTitle(Resources.GetString(Resource.String.commands) + " " + Resources.GetString(Resource.String.help).ToLower());
                            dialog.SetMessage(Resources.GetString(Resource.String.commandsHelp));
                            break;
                        case Fragments.Sensors:
                            dialog.SetTitle(Resources.GetString(Resource.String.sensors) + " " + Resources.GetString(Resource.String.help).ToLower());
                            dialog.SetMessage(Resources.GetString(Resource.String.sensorsHelp));
                            break;

                        case Fragments.Config:
                            dialog.SetTitle(Resources.GetString(Resource.String.config) + " " + Resources.GetString(Resource.String.help).ToLower());
                            dialog.SetMessage(Resources.GetString(Resource.String.configHelp));
                            break;
                    }
                    dialog.Show();
                    break;
            }
            return base.OnOptionsItemSelected(item);
        }

        enum Fragments
        {
            Commands,
            Sensors,
            Config
        }
    }
}

