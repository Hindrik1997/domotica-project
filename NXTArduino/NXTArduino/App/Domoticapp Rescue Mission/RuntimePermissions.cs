using System;
using System.Collections.Generic;
using Android.App;
using Android.OS;

namespace RuntimePermissions
{
    public class Permission
    {
        Activity givenActivity;

        public Permission(Activity activity)
        {
            givenActivity = activity;
        }

        public bool Check(string permission, int requestCode)
        {
            return Check(new string[] { permission }, requestCode);
        }

        public bool Check(string[] permissions, int requestCode)
        {
            List<string> permissionsToRequest = new List<string>();

            Console.WriteLine("Permission info: ");
            Console.WriteLine("Build version: " + Build.VERSION.Sdk);

            if(Convert.ToInt32(Build.VERSION.Sdk) >= 23)
            {
                //Get runtime permission
                for (int i = 0; i < permissions.Length; i++)
                {
                    Console.WriteLine("Requested permission: " + permissions[i]);
                    if (givenActivity.CheckSelfPermission(permissions[i]) != Android.Content.PM.Permission.Granted)
                    {
                        permissionsToRequest.Add(permissions[i]);
                    }
                }

                if(permissionsToRequest.Count == 0)
                {
                    return true;
                }
                else
                {
                    Request(permissionsToRequest.ToArray(), requestCode);
                    return false;
                }
            }
            else
            {
                //No need to request permission
                return true;
            }
        }

        private void Request(string[] permissions, int requestCode)
        {
            givenActivity.RequestPermissions(permissions, requestCode);
        }
    }
}