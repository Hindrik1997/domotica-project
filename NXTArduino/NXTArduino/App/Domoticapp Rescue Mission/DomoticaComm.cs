using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace Domoticapp_Rescue_Mission
{
    public class ReceiveSend
    {
        public bool shouldRun = true;
        public bool shouldReconnect = false;
        public LoginActivity loginActivity;
        public MainActivity mainActivity;
        Socket socket;
        IPAddress ip;
        byte[] receiveBuffer = new byte[8];
        Queue<byte[]> dataQueue = new Queue<byte[]>();
        byte[] sendData = new byte[8];
        public Thread commThread;


        public ReceiveSend(IPAddress IP)
        {
            ip = IP;
            Console.WriteLine("Starting ReceiveSend.");
            Console.WriteLine("Socket connected.");
            commThread = new Thread(BackgroundCommunication);
            commThread.Start();
        }

        public void Kill()
        {
            commThread.Abort();
            if (socket.Connected)
            {
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
            }
            socket = null;
        }

        private void BackgroundCommunication()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Console.WriteLine("Connecting...");
            try { socket.Connect(ip, 240); } catch (Exception e) { Console.WriteLine(e.Data); };
            socket.NoDelay = true;

            if (socket.Connected)
            {
                while (shouldRun)
                {
                    if (shouldReconnect)
                    {
                        lock (socket)
                        {
                            socket.Shutdown(SocketShutdown.Both);
                            socket.Close();
                            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                            socket.Connect(ip, 240);
                            shouldReconnect = false;
                        }
                    }

                    lock (dataQueue)
                    {
                        if (dataQueue.Count > 0)
                        {
                            socket.Send(dataQueue.Dequeue());
                        }
                    }

                    while (socket.Available > 0)
                    {
                        Console.WriteLine("receiving");
                        socket.Receive(receiveBuffer);
                        Console.WriteLine("received: " + receiveBuffer[0]);
                        if (Convert.ToChar(receiveBuffer[0]) == 'w')
                        {
                            loginActivity.SuccesfullyConnected();
                        }
                    }
                }
            }
        }

        public bool QueueData(char[] commands)
        {
            return QueueData(commands, -1);
        }

        public bool QueueData(char[] commands, short arg)
        {
            Console.WriteLine("Queueing data.");
            if (commands.Length > 4) return false;
            byte[] tempBuffer = new byte[8];
            lock (dataQueue)
            {
                tempBuffer = Encoding.ASCII.GetBytes(commands);

                dataQueue.Enqueue(tempBuffer);
                if (arg != -1)
                {
                    byte[] intStorage = BitConverter.GetBytes(arg);

                    for (int i = 0; i < intStorage.Length; i++)
                    {
                        Console.WriteLine("intbyte: " + intStorage[i]);
                    }
                    dataQueue.Enqueue(intStorage);
                }
                return true;
            }
        }

    }
}