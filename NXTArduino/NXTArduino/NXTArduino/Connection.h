#ifndef CONNECTION_H
#define CONNECTION_H

//Abstract, deze houd de status bij van de ethernet initialisatie
class Connection
{
public:
	Connection() {}
	inline virtual ~Connection() = 0; //Puur virtuele destructor. Zorgt ervoor dat Connection abstract is. Deze class is hierbij vergelijkbaar met een interface in c#
	static bool isInitialized; //Voor het bijhouden van het initialisern van de ethernet hardware.
};

//inline want is leeg 
inline Connection::~Connection() //Er moet alsnog een default implementatie zijn, ondanks dat de destructor puur virtueel is.
{	
}

//Delete het connection object op de gespecificeerde pointer. Ik gebruik hier een base class pointer zodat ik niet voor elke afgeleide class een aparte variant hoef te maken.
inline void DeleteConnection(Connection* pCon) 
{
	delete pCon;
}



#endif