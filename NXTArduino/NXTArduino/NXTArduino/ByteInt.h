#ifndef BYTE_INT_H
#define BYTE_INT_H

//Union om snel een int te splitten in bytes. Scheelt ook weer geheugen
//NOTITIE: Een int is een 16 bits in op de arduino én de NXT
union ByteInt {
	byte bArray[sizeof(int)];
	int intValue;
};


#endif

