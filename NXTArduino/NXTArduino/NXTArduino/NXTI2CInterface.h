#ifndef NXTI2CINTERFACE_H
#define NXTI2CINTERFACE_H

#include <Arduino.h> //Arduino libs VÓÓR commands.h
#include "ByteInt.h"
#include "CommandManager.h"
#include "Commands.h"
#include "Wire.h"

class NXTI2CInterface
{
public:
	NXTI2CInterface(bool);
	~NXTI2CInterface();
	void SetAsHost();
	void SetAsClient(byte);
	
	//Commandbuffer, zie CommandManager.h en CommandManager.cpp
	CommandManager* cmd;
	//Message welke zo spoedig mogelijk verzonden wordt.
	byte NextMessage[8];


	static NXTI2CInterface* CurrentStandardInterface;
private:
	static void ReceiveI2CInterrupt(int);
	static void RequestI2CInterrupt();
public:
	//Speciaal voor de interrupt, aka de Current Standard Interface. 
	//Deze functies zijn zo klein dat ze wss inline uitgevouwd kunnen worden.
	//Dit scheelt natuurlijk weer in de performance, aangezien hij niet eerst de functiepointer uit het geheugen hoeft te halen etc.
	//Met het inline keyword geef je de compiler hier een hint voor
	//Hiervoor moeten de functie definities echter in dezelfde translation unit zitten, (Deze header file)
	inline void SetAsCurrentStandardInterface();
	inline static void SetCurrentStandardInterface(NXTI2CInterface*);

	//Commando's, ook allemaal klein en dus inline hint. de GCC Compiler bepaalt wel of t ook echt inline wordt.
	inline void StartMotor(byte, byte, byte);
	inline void StopMotor(byte);
	inline void TurnMotor(byte, byte, ByteInt);

	//Motors, ook hier weer inline.
	inline void StartMotorA(byte,byte);
	inline void StartMotorB(byte,byte);
	inline void StartMotorC(byte,byte);
	inline void StopMotorA();
	inline void StopMotorB();
	inline void StopMotorC();
	inline void SetTrainSwitchStop();
	inline void SetTrainSwitchForward();
	inline void SetTrainSwitchBackward();
	inline void TurnMotorA(byte, byte, ByteInt);
	inline void TurnMotorB(byte, byte, ByteInt);
	inline void TurnMotorC(byte, byte, ByteInt);
};





//INLINES

inline void NXTI2CInterface::SetAsCurrentStandardInterface()
{
	CurrentStandardInterface = this;
}

inline void NXTI2CInterface::SetCurrentStandardInterface(NXTI2CInterface * standardInterface)
{
	CurrentStandardInterface = standardInterface;
}


//Commando functies, ook allemaal inline.
inline void NXTI2CInterface::StartMotor(byte MotorPort, byte Direction, byte Power)
{
	byte msg[8] = { COMMAND_TURN_MOTOR, MotorPort, Direction, Power };
	cmd->AddMsg(msg);
}

inline void NXTI2CInterface::StartMotorA(byte Direction, byte Power)
{
	byte msg[8] = { COMMAND_TURN_MOTOR, COMMAND_SELECT_MOTORA, Direction, Power };
	cmd->AddMsg(msg);
}

inline void NXTI2CInterface::StartMotorB(byte Direction, byte Power)
{
	byte msg[8] = { COMMAND_TURN_MOTOR, COMMAND_SELECT_MOTORA, Direction, Power };
	cmd->AddMsg(msg);
}

inline void NXTI2CInterface::StartMotorC(byte Direction, byte Power)
{
	byte msg[8] = { COMMAND_TURN_MOTOR, COMMAND_SELECT_MOTORA, Direction, Power };
	cmd->AddMsg(msg);
}

inline void NXTI2CInterface::StopMotor(byte MotorPort)
{
	byte msg[8] = { COMMAND_STOP_MOTOR, MotorPort };
	cmd->AddMsg(msg);
}

inline void NXTI2CInterface::TurnMotor(byte motor, byte direction, ByteInt angle)
{
	byte msg[8] = { COMMAND_TURN_MOTOR, motor, direction, angle.bArray[0], angle.bArray[1] }; //Een int is een 16 bits in op de arduino én de NXT
	cmd->AddMsg(msg);
}

inline void NXTI2CInterface::StopMotorA()
{
	byte msg[8] = { COMMAND_STOP_MOTOR, COMMAND_SELECT_MOTORA };
	cmd->AddMsg(msg);
}
inline void NXTI2CInterface::StopMotorB()
{
	byte msg[8] = { COMMAND_STOP_MOTOR, COMMAND_SELECT_MOTORB };
	cmd->AddMsg(msg);
}
inline void NXTI2CInterface::StopMotorC()
{
	byte msg[8] = { COMMAND_STOP_MOTOR, COMMAND_SELECT_MOTORC };
	cmd->AddMsg(msg);
}

inline void NXTI2CInterface::TurnMotorA(byte power, byte direction, ByteInt angle)
{
	byte msg[8] = { COMMAND_TURN_MOTOR_ANGLE, COMMAND_SELECT_MOTORA,power, direction, angle.bArray[0],angle.bArray[1] };
	cmd->AddMsg(msg);
}

inline void NXTI2CInterface::TurnMotorB(byte power, byte direction, ByteInt angle)
{
	byte msg[8] = { COMMAND_TURN_MOTOR_ANGLE, COMMAND_SELECT_MOTORB,power, direction, angle.bArray[0], angle.bArray[1] };
	cmd->AddMsg(msg);
}

inline void NXTI2CInterface::TurnMotorC(byte power,byte direction, ByteInt angle)
{
	byte msg[8] = { COMMAND_TURN_MOTOR_ANGLE, COMMAND_SELECT_MOTORC,power, direction, angle.bArray[0], angle.bArray[1] };
	cmd->AddMsg(msg);
}


inline void NXTI2CInterface::SetTrainSwitchStop() 
{
	byte msg[8] = { COMMAND_SET_TRAIN_SWITCH, TRAIN_DISABLED };
	cmd->AddMsg(msg);
}

inline void NXTI2CInterface::SetTrainSwitchForward()
{
	byte msg[8] = { COMMAND_SET_TRAIN_SWITCH, TRAIN_FORWARD };
	cmd->AddMsg(msg);
}

inline void NXTI2CInterface::SetTrainSwitchBackward()
{
	byte msg[8] = { COMMAND_SET_TRAIN_SWITCH, TRAIN_BACKWARD };
	cmd->AddMsg(msg);
}



#endif

