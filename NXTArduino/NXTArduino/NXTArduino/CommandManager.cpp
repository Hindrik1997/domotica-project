#include "CommandManager.h"
#include "NXTI2CInterface.h"

//Constructor
CommandManager::CommandManager(NXTI2CInterface* ic) : cInterface(ic) //Initialization list ivm default constructor van NXTI2CInterface class
{
}

//Stelt het volgende commando in
void CommandManager::SetNextMsg()
{
	if (CommandsInBuffer == 0)
		return;
	memcpy(cInterface->NextMessage, commandbuffer, 8);
	CommandsInBuffer--;
}

//Hierme kun je commando's toevoegen. Indien er plek is in de interface, wordt hij direct daarin gezet. Anders wordt ie in de buffer gezet.
//Ja, byte input[8] is gelijk aan byte*, maar dit is ter verbetering van het lezen van de code.
void CommandManager::AddMsg(byte input[8])
{
	if (cInterface->NextMessage[0] == 0)
		memcpy(cInterface->NextMessage, input, 8);
	else
	{
		CommandsInBuffer++;
		memcpy(commandbuffer,input,8);
	}
}

