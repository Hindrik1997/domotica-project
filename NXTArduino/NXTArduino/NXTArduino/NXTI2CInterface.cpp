#include "NXTI2CInterface.h"

// WAARSCHUWING: NIET ALLE FUNCTIE DEFINITIES ZITTEN HIER, INLINES ZITTEN IN DE HEADER!

//CurrentStandardInterface gelijk zetten aan een nullptr.
NXTI2CInterface* NXTI2CInterface::CurrentStandardInterface = nullptr;

//Constructor
NXTI2CInterface::NXTI2CInterface(bool isHost = false)
{
	cmd = new CommandManager(this);
	SetAsCurrentStandardInterface();
	Wire.onReceive(ReceiveI2CInterrupt);
	Wire.onRequest(RequestI2CInterrupt);
	if (isHost)
	{
		Wire.begin();
	}
	else
	{
		Wire.begin(NXT_BUS);
	}
}

//Destructor
NXTI2CInterface::~NXTI2CInterface()
{
	//commandmanager (de buffer) opruimen
	if(cmd != nullptr)
	delete cmd;
}

void NXTI2CInterface::SetAsHost()
{
	Wire.end();
	Wire.begin();
}

void NXTI2CInterface::SetAsClient(byte Address = NXT_BUS)
{
	Wire.end();
	Wire.begin(Address);
}

void NXTI2CInterface::ReceiveI2CInterrupt(int BytesIn)
{
	//De arduino functioneert als slave, maar stuurt wel de commandos.
	//Hiervoor 'polled' de NXT de arduino steeds, en vraagt 8 bytes terug.
	//BytesIn kan ik hier ignoren, is voor interrupt nodig
	//Doet eigenlijk niks momenteel, is niet nodig. 
	//Puur code voor evt. latere extensie
}

//Regelt de reply's wanneer de NXT een message stuurt. 
//De NXT kan enkel als Master werken, vandaar dat de NXT de Arduino polled. Deze functie dereferenced de pointer en haalt de message op.
//Vervolgens called ie SetNextMsg() voor de volgende message. Deze functie had veel problemen ivm de beperkte tijd waarin je een reply kan sturen
//richting de NXT.
void NXTI2CInterface::RequestI2CInterrupt()
{
	Wire.write(CurrentStandardInterface->NextMessage, 8);
	memset(CurrentStandardInterface->NextMessage,0,8);
	CurrentStandardInterface->cmd->SetNextMsg();
}

