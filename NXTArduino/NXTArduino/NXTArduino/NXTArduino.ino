/*
 Name:		NXTArduino.ino
 Created:	12/18/2015 8:39:42 PM
 Author:	Hindrik
*/

//Custom libraries
#include <SPI.h>
#include <Ethernet.h>
#include <Wire.h>
#include "NXTI2CInterface.h"
#include "ClientConnection.h"

//NXTI2CInterface initialized zichzelf
NXTI2CInterface i2c(false);

//ClientConnection doet dit niet
ClientConnection* ClientCon;

/*
int freeRam() { //functie om de vrije heap space te checken
	extern int __heap_start, *__brkval;
	int v;
	return (int)&v - (__brkval == 0 ? (int)&__heap_start : (int)__brkval);
}


#ifdef SERVER
	ServerConnection* con;
#else
	ClientConnection* con;
#endif
	bool lamp = false;

	*/

void setup() {
	/*
	#ifdef SERVER
		con = new ServerConnection(PORT);
	#else
		con = new ClientConnection(Arduino::NXTBuddy);
	#endif
	
		pinMode(13, OUTPUT);
		*/
	pinMode(8, INPUT);
	ClientCon = new ClientConnection(Arduino::NXTBuddy);
}

bool DepartsFromLoader = false;

// the loop function runs over and over again until power down or reset
void loop() {

/*
#ifndef SERVER
{
	Message msg = con->GetAvailableMessage();

		

	if (msg.isValid)
	{
		Serial.println("SWITCH");
		digitalWrite(13, lamp);
		lamp = !lamp;
	}
}
	byte msgd[] = { 0 };
	Message msg2(Arduino::NXTBuddy, Arduino::Server, msgd, (byte)1);
	con->SendMessage(msg2);
	delay(1000);
#endif
#ifdef SERVER
	{
		Message msg = con->GetAvailableMessage();
		if (msg.receiver == Arduino::Server)
		{
			Serial.println("SWITCH");
			digitalWrite(13, lamp);
			lamp = !lamp;
			byte msgd2[] = { 0 };
			Message msgr(Arduino::Server, Arduino::NXTBuddy, msgd2, (byte)1);
			con->SendMessage(msgr);
		}
	}
#endif
*/

	//Messages afhandelen
	Message msg = ClientCon->GetAvailableMessage();
	if (msg.isValid)
	{
		switch (msg.message[0])
		{
		case 't':
		{
			if (msg.message[1] == 1)
				NXTI2CInterface::CurrentStandardInterface->SetTrainSwitchStop();
			else
			{
				if (msg.message[1] == 2)
				{
					if (msg.sender == Arduino::Laadstation)
						DepartsFromLoader = true;
					NXTI2CInterface::CurrentStandardInterface->SetTrainSwitchForward();
				}
				else
					NXTI2CInterface::CurrentStandardInterface->SetTrainSwitchBackward();
			}
			break;
		}
		default:
			break;
		}
	}

	if (DepartsFromLoader)
	{
		if (digitalRead(8) == LOW)
		{
			//Trein is eindstation binnegekomen
			DepartsFromLoader = false;
			NXTI2CInterface::CurrentStandardInterface->SetTrainSwitchStop();
			
			ByteInt Angle;
			Angle.intValue = 45;
			NXTI2CInterface::CurrentStandardInterface->TurnMotorA(100, DIRECTION_COUNTERCLOCKWISE, Angle);
			NXTI2CInterface::CurrentStandardInterface->SetTrainSwitchBackward();
			delay(5000);
			NXTI2CInterface::CurrentStandardInterface->SetTrainSwitchStop();
		}
	}

	//8 is sensor
	

	

}