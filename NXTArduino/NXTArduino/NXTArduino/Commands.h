//Header guard
#ifndef COMMANDS_H
#define COMMANDS_H

//typedefje vanwege compatibiliteit tussen NXT en Arduino, NXT kent geen byte type, Arduino libs wel.
typedef byte uint_8;

//Indien dit op de arduino draait is de PROGMEM directive gedefinieerd.
//PROGMEM slaat constanten op in het grotere geheugen van de arduino. Dit is efficienter qua ram.
//De NXT controller kent dit niet, maar aangezien ik dit liever een shared header wil houden, kies ik voor een macro constructie
//De arduino libs definieren PROGMEM, dus wanneer je deze header include moet je dit altijd NA de arduino lib doen.
#ifndef PROGMEM 
	#define PROGMEM //Lege PROGMEM, dus wordt vervangen door niks.
#endif

//Bus adressen voor de I2C Interface
const PROGMEM byte NXT_BUS = 0x0A; //Hardware geconfigureerde adres van de NXT
const PROGMEM byte ARDUINO_BUS = 0x14; //En adres van de Arduino

//Command types
const PROGMEM byte COMMAND_TURN_MOTOR = 0x01;
const PROGMEM byte  COMMAND_STOP_MOTOR = 0x02;
const PROGMEM byte COMMAND_SELECT_PORT = 0x03;
const PROGMEM byte COMMAND_TURN_MOTOR_ANGLE = 0x04;
const PROGMEM byte COMMAND_SET_TRAIN_SWITCH = 0x05;

//Port selectors
const PROGMEM byte COMMAND_SELECT_MOTORA = 0x0A;
const PROGMEM byte COMMAND_SELECT_MOTORB = 0x0B;
const PROGMEM byte COMMAND_SELECT_MOTORC = 0x0C;

const PROGMEM byte COMMAND_SELECT_PORT1 = 0x01;
const PROGMEM byte COMMAND_SELECT_PORT2 = 0x02;
const PROGMEM byte COMMAND_SELECT_PORT3 = 0x03;
const PROGMEM byte COMMAND_SELECT_PORT4 = 0x04;

//Motor richtingen
const PROGMEM byte DIRECTION_CLOCKWISE = 0x00;
const PROGMEM byte DIRECTION_COUNTERCLOCKWISE = 0x01;

//Voor gedefinieerde snelheden voor motoren, je kan zelf ook getallen kiezen. (Tussen -100 en 100)
const PROGMEM byte MOTOR_SPEED_LOW = 0x19;
const PROGMEM byte MOTOR_SPEED_MIDDLE = 0x32;
const PROGMEM byte MOTOR_SPEED_HIGH = 0x4B;
const PROGMEM byte MOTOR_SPEED_HIGHEST = 0x64;

//Trein richtingen
const PROGMEM byte TRAIN_BACKWARD = 0x00;
const PROGMEM byte TRAIN_DISABLED = 0x01;
const PROGMEM byte TRAIN_FORWARD = 0x02;


/*
FORMATS:

TURN_MOTOR:
SELECT_MOTOR, DIRECTION, POWER
STOP_MOTOR:
SELECT_MOTOR
TURN_MOTOR_ANGLE:
SELECT_MOTOR,POWER, DIRECTION, ANGLE
COMMAND_SET_TRAIN_SWITCH:
TRAIN_DIRECTION

*/

#endif