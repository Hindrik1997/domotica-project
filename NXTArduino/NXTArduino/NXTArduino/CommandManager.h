//Header guard
#ifndef COMMANDMANAGER_H
#define COMMANDMANAGER_H

#include "Arduino.h"
//Forward declaration van de NXTI2CInterface Class, incomplete type, is nodig vanwege een circulaire dependency
class NXTI2CInterface;

//Deze class wordt gebruikt om commando's in te bufferen. Normaal gesproken zou ik een template queue class bouwen, of std::queue gebruiken, maar 
// dat kan niet op de Arduino, en een custom template class bleek te lanzgaam. De I2C bus vereist namelijk een reply binnen een bepaalde tijd
// en daar moet je binnen blijven. Een directe pointer en memcpy bleek de beste oplossing qua tijd.
class CommandManager
{
public:
	CommandManager(NXTI2CInterface*);
	byte commandbuffer[8];
	byte CommandsInBuffer = 0;
	NXTI2CInterface* cInterface;
	void SetNextMsg();
	void AddMsg(byte input[8]);
};

#endif

