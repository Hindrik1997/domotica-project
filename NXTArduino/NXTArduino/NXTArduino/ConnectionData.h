#include <Arduino.h>
#include <avr/pgmspace.h>
#ifndef CONNECTION_DATA_H
#define CONNECTION_DATA_H

//constants met een PROGMEM directive voor het opslaan in het programmageheugen
const PROGMEM byte SERVER_MAC[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
const PROGMEM byte NXTBUDDY_MAC[] = { 0x98, 0x4F,0x54,0x85,0xF2,0xCD };
const PROGMEM byte LAADSTATION_MAC[] = { 0x21, 0x68, 0xD3, 0x05, 0x1C, 0xC8 };
const PROGMEM byte MEETSTATION_MAC[] = { 0x37, 0x9A, 0x82, 0xFA, 0x4F, 0xE8 };

const PROGMEM byte SERVER_IP[] = { 192,168,1,10 };
const PROGMEM byte NXTBUDDY_IP[] = { 192,168,1,12 };
const PROGMEM byte LAADSTATION_IP[] = { 192,168,1,11 };
const PROGMEM byte MEETSTATION_IP[] = { 192,168,1,13 };

//Poort waar we over werken.
const PROGMEM byte PORT = 240;

//Startbyte voor verificatie
const PROGMEM byte StartByte = 254;

//Devices in ons systeem. Hoofdzakelijk arduino's.
enum class Arduino : byte
{
	Null,
	Server,
	NXTBuddy,
	Laadstation,
	Meetstation,
	Phone
};

//Speciale enum voor de Messagetype ivm de allocaten van de arrays.
enum class MessageType : byte 
{
	Normal,
	SecondConst,
	ThirdConst
};

#endif

//Voetnoot over c++ en c#

/*

*
C++ zijn object allocation gebeurd afhankelijk van de syntax. Een class is dan ook bijna t zelfde als een struct.
In tegen stelling tot C# waar class vs struct identiek is aan reference of value type.
Met de new() operator maak je een new object op de heap. Anders initialiseer je m automatisch op de stack.

Message(false);  - Stack
Message* pMsg = new Message(false); - Heap

Belangrijk verschil is ook dat je in C++ zelf de heap moet managen in tegenstelling tot C# waarbij de CLR met zn garbage collector dat voor je doet.
Dit gaat doormidel van pointers. Ook c# kent pointers, maar hierbij kan je ze niet laten 'memoryleaken' als in C#. In C# kan je hooguit zelf
met pointers werken als een soort van variabeles die references vasthouden. Zodra je klaar bent wordt de controle over het geheugen automatisch teruggegeven aan de GC.

In C++ is een array daarnaast ook een apart beestje. Je hebt er namelijk 2 soorten. Statsiche stack allocated arrays en dynamische heap allocated arrays.
Stack allocated arrays moeten een compile time constante als grootte hebben, en zijn daardoor niet zo flexibel. Dynamische arrays hebben dat probleem niet.
Ze hebben echter wel het probleem dat je zelf het geheugen moet managen dmv pointers en delete statements.
Eigenlijk is een array niet veel meer dan een pointer naar t eerste element. Je moet altijd de lengte meegeven tenzij
je met een terminator element werkt. (C-Strings) Een uitzondering hierop is ook statische arrays in local scope, aangezien je de lengte met de sizeof() operator kan opvragen.
Ga je echter de array meegeven aan een functie dan wordt t niets meer dan een ordinaire pointer.


In C++ is n struct dan ook simpelweg een class waarbij de members defaulten naar een public acces modifier, en een class is by default een private modifier.

Heel belangrijk in C++ is ook de scheiding van declaratie van definitie, terwijl die in c# een en t zelfde is.
Over het algemeen zitten de declaraties in de header files (.h/.hpp) en de werkelijke definities in de source files. (.c/.cpp)
Dit is om conflicten te voorkomen voor de linker. Er zijn uitzonderingen op deze regels. In sommige gevallen, inline en template functies/class, bevindt zich wel een definitie in de header.
Daarnaast uiteraard het verschil qua compilatie, waarbij c++ alles direct naar machine code wordt gecompileerd en bij c# naar IL, (Intermediate Language)
welke vervolgens door de CLR tijdens runtime wordt verwerkt tot machine code. De CLR is eigenlijk dan ook gewoon een C++ applicatie die dat doet.
Sterker nog, je kan je kan je eigen CLR en de gehele .net runtime draaien binnen je eigen c++ app dmv COM objecten.

*/