#include "ServerConnection.h"

ServerConnection::ServerConnection(int Port) : server(Port)
{
	Serial.end();
	Serial.begin(9600);
	if (!Connection::isInitialized)
	{
		IPAddress ip(SERVER_IP[0], SERVER_IP[1], SERVER_IP[2], SERVER_IP[3]);
		byte mac[6] = { SERVER_MAC[0], SERVER_MAC[1], SERVER_MAC[2], SERVER_MAC[3], SERVER_MAC[4], SERVER_MAC[5] };
		Ethernet.begin(mac, ip); //Const_Cast werkt hier niet aangezien alles in t programma geheugen const moet zijn.
		Connection::isInitialized = true;
	}
	
	Serial.print(F("IP Address is:"));
	Serial.println(Ethernet.localIP());
	server.begin();
}

ServerConnection::~ServerConnection()
{
}

//Deze messages worden gedecodeerd afgeleverd! message.message levert dus puur de data op!
Message ServerConnection::GetAvailableMessage()
{
	EthernetClient client = server.available();
	if (client)
	{	
		byte GetStartByte = 0;
		while (GetStartByte != StartByte)
			GetStartByte = client.read();


		byte tArray[3];
		for (byte i = 0; i < 3; ++i)
		{
			tArray[i] = client.read();
		}
		byte Length = tArray[2];

		byte* msgData = new byte[Length];

		for (byte i = 0; i < Length; ++i) 
		{
			msgData[i] = client.read();
		}

		byte* rawMessage  = new byte[Length + 3];

		for (byte i = 0; i < 3; ++i)
		{
			rawMessage[i] = tArray[i];
		}

		for (byte i = 0; i < Length; ++i)
		{
			rawMessage[i + 3] = msgData[i];
		}
		delete[] msgData;

		Message rMessage(rawMessage, Length+3);

		if (rMessage.receiver != Arduino::Server)
		{
			rMessage.Reserialize();
			SendMessage(rMessage);
			return Message(false);
		}
		else
			return rMessage;
	}
	else
	{
		return Message(false);
	}

}