#ifndef MESSAGE_H
#define MESSAGE_H
#include <Arduino.h>
#include "ConnectionData.h"

class Message
{
public:
	Message(Arduino, Arduino, byte*,byte); //Constructor om een message aan te maken. Deze wordt geserializeerd.
	Message(byte*,byte); //Constructor voor het deserializeren van een message
	Message(const Message& cSource); //Copy constructor overload voor deep copy
	Message& operator=(const Message&); //Assignment operator overloaden voor deep copy
	inline Message(bool); //Voor uitleg inlining zie andere header files.
	inline ~Message();
public:
	byte* message;
	byte* MsgToSend;
	Arduino receiver; //Ontvanger
	Arduino sender; //Verzender
	byte length; //Message data array lengte. Exclusief de sender,receiver en de lengte zelf.
	bool isValid = false; //Check vanwege het feit dat ik niet met pointers wou werken ivm de anderen, en het feit dat C++'s objecten niet null kunnen zijn.
public:
	void Serialize(); //zorgt voor omzetten van de data en ontvanger enz. naar een message die klaar is om te worden verzonden.
	void Deserialize(); //Het omgekeerde van hierboven
	void Reserialize(); //Voor het herserializeren van messages. Delete de msgtosend!
	MessageType msgType = MessageType::Normal;
private:
	inline void Destroy();
};

inline Message::Message(bool isvalid = false) : isValid(isvalid),msgType(MessageType::ThirdConst), MsgToSend(nullptr), message(nullptr), receiver(Arduino::Null),sender(Arduino::Null), length(0)
{
}

inline void Message::Destroy() //Ruimt de byte arrays op die anders op de heap zouden blijven. 
{							   //Ik heb hier een destructor op basis van de msgType, dit ivm het gebruik voor de anderen, 
								//zodat ze geen byte* voor een array op de heap hoeven te gebruiken maar gwn n statische array.
								//Niet de mooiste manier, maar ik vond het belangrijker dat zij er goed mee kunnen werken dan dat de code mooi is.
	if (msgType != MessageType::ThirdConst) //NullMessage, dan hoeft ie sws niks te deallocaten want zijn toch nullptr
	{
		if (MsgToSend != nullptr)
		{
			delete[] MsgToSend; //Delete de array
		}
		if (msgType == MessageType::SecondConst && message != nullptr)
		{
			delete[] message; //En weg er mee!
		}
	}
}

inline Message::~Message() //Destructor
{
	Destroy();
}

#endif