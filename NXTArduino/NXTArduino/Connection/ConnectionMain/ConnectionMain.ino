/*
 Name:		Connection.ino
 Created:	1/17/2016 8:26:00 PM
 Author:	Hindrik
*/

#include <Ethernet.h> //Vereist!
#include <SPI.h> //SPI.h is vereist!

//Deze twee headers moet je includen in je .ino
#include "ServerConnection.h"
#include "ClientConnection.h"

//Als je een server connectie wilt:
ServerConnection* ServerCon;

//Als je een client connectie wilt:
ClientConnection* ClientCon;

void setup() {
	//In setup initialiseer je de server / client

	ServerCon = new ServerConnection(PORT); 
	/*
	Enige argument is de poortnummer waarop je wilt hosten.
	Deze zijn gedefinieerd in ConnectionData.h
	*/

	ClientCon = new ClientConnection(Arduino::NXTBuddy);
	/*Enige argument hier is welke Arduino de client is.*/

	/*  Heel belangrijk is dat dit pointers zijn, omdat je niet allerlei code voor de setup kan doen.
		Blijf hier asjeblieft vanaf.
		Je moet zo nu en dan eens de connection resetten.
		Dit is vooral verstandig als je meer dan 10 seconden geleden een bericht hebt verzonden.
		Dit kan je doen met Connection.Reset() */
}

void loop() {
	/*
	Het systeem werkt doormiddel van messages.

	Als je een message maakt dan moet je eerst aangeven van wie de message komt, bijv Arduino::NXTBuddy
	Vervolgens geef je de ontvanger aan.
	Hierna geef je de de byte array.
	Als laatste stop je de lengte van de byte array er in.

	(In C++ houdt een array zijn lengte niet bij. Er is dus niet iets als array.length,
	echter als je de byte array op de juiste manier declareert kan je wel 'sizeof(array)' gebruiken)
	
	Zorg ervoor dat je de byte array op de volgende manier declareert, en niet dmv de new operator.
	*/

	byte arrayMetData[5] = { 0,23,42,52,32 };
	/*
	Een message maak je als volgt:
	*/

	Message msg(Arduino::Meetstation, Arduino::Phone, arrayMetData, sizeof(arrayMetData) );
		

	/* Deze verzend je dan vervolgens als volgt: */

	ClientCon->SendMessage(msg);

	/*
		Als je wilt checken of er messages voor je klaar staan kan dat ook. De geretouneerde data kan uitgelezen worden met 'returnmessage.message'
		Deze functie retourneert enkel messages voor deze Arduino
	*/

	Message msg2 = ClientCon->GetAvailableMessage(); //Message opvragen.
	if (msg2.isValid) //Checken of t een geldige message is
	{
		//Hier kun je het resultaat lezen als een array van bytes.

		byte data1 = msg2.message[0];
		byte data2 = msg2.message[1]; //etc.
	}

	//Het is verstandig om je connectie te resetten als je de connectie ff niet gebruikt.
	//Dit doe je door m eerst te deleten, en dan een nieuwe aan te maken als in void setup()

	DeleteConnection(ClientCon);

	//Vervolgens uiteraard weer een nieuwe aanmaken:
	ClientCon = new ClientConnection(Arduino::Meetstation);
}
