#ifndef CONNECTION_H
#define CONNECTION_H

//Abstract, deze houd de status bij van de ethernet initialisatie
class Connection
{
public:
	Connection() {}
	inline virtual ~Connection() = 0; //Puur virtuele destructor. Zorgt ervoor dat Connection abstract is. Deze class is hierbij vergelijkbaar met een interface in c#
	static bool isInitialized;
};

//Er moet alsnog een default implementatie zijn, ondanks dat de destructor puur virtueel is.
//Dit ivm de destructor call chain bij het destructen van een deriviate class.
//Anders krijg je linker errors. Ik heb m ge-inlined want deze functie doet verder toch weinig.
//inline is enkel een hint aan de compiler om te kijken of deze functie ook echt inline moet zijn. (inline wil zeggen dat de functiecall inline geexpandeerd wordt.)
inline Connection::~Connection() 
{	
}

#endif