#ifndef SERVER_CONNECTION_H
#define SERVER_CONNECTION_H

#include <Ethernet.h>
#include "ConnectionData.h"
#include "Connection.h"
#include "Message.h"

class ServerConnection : public Connection //publieke inheritance van de Connection class
{
public:
	ServerConnection(int);
	~ServerConnection();
	inline void SendMessage(Message&); //Kan prima ge inlined worden, is n superkleine functie.
	//Met het inline keyword hint je de compiler op een inline expansie van de functiecall.
	//Dit scheelt weer een geheugen acces en dus performance.

	Message GetAvailableMessage();
protected:
	EthernetServer server;
};

inline void ServerConnection::SendMessage(Message& msg) //neemt een Message&, (Message reference). Een soort makkelijke vorm van een pointer. Vergelijkbaar met C#'s references*
{
	server.write(msg.MsgToSend, msg.length + 4);
}

#endif