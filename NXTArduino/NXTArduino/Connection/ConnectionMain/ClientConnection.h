#ifndef CLIENT_CONNECTION_H
#define CLIENT_CONNECTION_H

#include "Connection.h"
#include "ConnectionData.h"
#include "Message.h"
#include <Ethernet.h>

//Client connectie class om de connecties voor de client arduino's te regelen.
class ClientConnection : public Connection
{
public:
	ClientConnection(Arduino);
	~ClientConnection();
public:
	Arduino thisClient;
	EthernetClient client;
public:
	inline void SendMessage(Message&);
	Message GetAvailableMessage();
};

//inlining van de functie aangezien hij maar heel klein is.
//het inline keyword geeft de compiler een hint om te kijken of de functie inline, dwz dat hij inline geexpandeerd wordt, moet worden.
inline void ClientConnection::SendMessage(Message& msg)
{
	client.write(msg.MsgToSend, msg.length + 4);
	client.flush();
}

#endif