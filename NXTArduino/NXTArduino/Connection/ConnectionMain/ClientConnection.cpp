#include "ClientConnection.h"

//Cre�rt het ClientConnection object dmv het kiezen van de arduino waar deze connectie zich op bevind.
ClientConnection::ClientConnection(Arduino ThisClient) : thisClient(ThisClient)
{
	Serial.end();
	Serial.begin(9600);

	if (!Connection::isInitialized)
	{
		switch (thisClient) 
		{
		case Arduino::NXTBuddy:
		{
			IPAddress ip(NXTBUDDY_IP[0], NXTBUDDY_IP[1], NXTBUDDY_IP[2], NXTBUDDY_IP[3]);
			byte mac[6] = { NXTBUDDY_IP[0], NXTBUDDY_IP[1], NXTBUDDY_IP[2], NXTBUDDY_IP[3], NXTBUDDY_IP[4], NXTBUDDY_IP[5] };
			Ethernet.begin(mac, ip); //Const_Cast werkt hier niet aangezien alles in t programma geheugen const moet zijn.			
			break;
		}
		case Arduino::Laadstation:
		{
			IPAddress ip2(LAADSTATION_IP[0], LAADSTATION_IP[1], LAADSTATION_IP[2], LAADSTATION_IP[3]);
			byte mac2[6] = { LAADSTATION_IP[0], LAADSTATION_IP[1], LAADSTATION_IP[2], LAADSTATION_IP[3], LAADSTATION_IP[4], LAADSTATION_IP[5] };
			Ethernet.begin(mac2, ip2); //Const_Cast werkt hier niet aangezien alles in t programma geheugen const moet zijn.
			break;
		}
		case Arduino::Meetstation:
		{
			IPAddress ip2(MEETSTATION_IP[0], MEETSTATION_IP[1], MEETSTATION_IP[2], MEETSTATION_IP[3]);
			byte mac2[6] = { MEETSTATION_IP[0], MEETSTATION_IP[1], MEETSTATION_IP[2], MEETSTATION_IP[3], MEETSTATION_IP[4], MEETSTATION_IP[5] };
			Ethernet.begin(mac2, ip2); //Const_Cast werkt hier niet aangezien alles in t programma geheugen const moet zijn.
			break;
		}
		default:
			Serial.println(F("Invalid arduino")); //De F() functie plaatst de string in het programmageheugen ipv in de ram.
			return;
		}
	}
	IPAddress ips(SERVER_IP[0],SERVER_IP[1],SERVER_IP[2],SERVER_IP[3]);
	Serial.print(F("Server ip will be: "));
	Serial.println(ips);
	Serial.print(F("Server port will be: "));
	Serial.println(PORT);
	if (client.connect(ips, PORT)) 
	{
		Serial.println(F("Connected to server"));
	}
	else
	{
		Serial.println(F("Connection failed"));
	}
}


ClientConnection::~ClientConnection()
{
}

//Haalt de beschikbare messages op. returnmessage.message is hier puur de data. Check voor een invalid message. (msg.isValid)
Message ClientConnection::GetAvailableMessage()
{
	int avBytes = client.available();
	if (avBytes > 0)
	{
		byte GetStartByte = 0;
		while (GetStartByte != StartByte)
			GetStartByte = client.read();

		byte tArray[3];
		for (byte i = 0; i < 3; ++i)
		{
			tArray[i] = client.read();
		}
		byte Length = tArray[2];

		byte* msgData = new byte[Length];

		for (byte i = 0; i < Length; ++i)
		{
			msgData[i] = client.read();
		}

		byte* rawMessage = new byte[Length + 3];

		for (byte i = 0; i < 3; ++i)
		{
			rawMessage[i] = tArray[i];
		}

		for (byte i = 0; i < Length; ++i)
		{
			rawMessage[i + 3] = msgData[i];
		}
		delete[] msgData;

		Message rMessage(rawMessage, Length + 3);	

		if (rMessage.receiver != thisClient)
		{
			return Message(false);
		}
		else
			return rMessage;
	}
	else
	{
		return Message(false);
	}
}