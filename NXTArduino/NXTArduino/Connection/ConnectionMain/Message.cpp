#include "Message.h"

//Zelf byte* managen. Aka altijd message op de stack maken
Message::Message(Arduino Sender, Arduino Receiver, byte* Message,byte Length) : sender(Sender), receiver(Receiver), message(Message), length(Length),MsgToSend(nullptr),msgType(MessageType::Normal), isValid(true)
{
	Serialize();
}

Message::Message(byte* rawMessage, byte totalLength) : MsgToSend(rawMessage), length(totalLength), msgType(MessageType::SecondConst),message(nullptr),isValid(true)
{
	Deserialize();
}

Message::Message(const Message& cSource) //Overloaded copy constructor
{
	if (cSource.isValid) 
	{
		length = cSource.length;
		receiver = cSource.receiver;
		sender = cSource.sender;
		msgType = cSource.msgType;
		isValid = cSource.isValid;

		if (cSource.message)
		{
			message = new byte[cSource.length];
			memcpy(message, cSource.message, length);
		}
		if (cSource.MsgToSend)
		{
			MsgToSend = new byte[cSource.length + 4];
			memcpy(MsgToSend, cSource.MsgToSend, length + 4);
		}
	}
}
//Overload van de = operator
Message& Message::operator=(const Message& cSource) 
{
	if (this == &cSource) 
	{
		return *this; //self assignment checkje
	}

	if (cSource.isValid)
	{
		length = cSource.length;
		receiver = cSource.receiver;
		sender = cSource.sender;
		msgType = cSource.msgType;
		isValid = cSource.isValid;

		if (cSource.message != nullptr)
		{
			message = new byte[cSource.length];
			memcpy(message, cSource.message, length);
		}
		if (cSource.MsgToSend != nullptr)
		{
			MsgToSend = new byte[cSource.length + 4];
			memcpy(MsgToSend, cSource.MsgToSend, length + 4);
		}
	}
	return *this;
}

//Serializatie functie
void Message::Serialize() 
{
	MsgToSend = new byte[length + 4];
	memcpy(MsgToSend + 4, message, length);
	MsgToSend[0] = StartByte;
	MsgToSend[1] = static_cast<byte>(sender);
	MsgToSend[2] = static_cast<byte>(receiver);
	MsgToSend[3] = length;
}

//Deserializatie functie
void Message::Deserialize() 
{
	length = length - 3;
	sender = static_cast<Arduino>(MsgToSend[0]);
	receiver = static_cast<Arduino>(MsgToSend[1]);
	message = new byte[length];
	memcpy(message,MsgToSend+3,length);
}

void Message::Reserialize()
{
	delete[] MsgToSend;
	Serialize();
}





//Formaat:
// Byte		1			2			3			4+
//		   sender	receiver	  lengte	  message